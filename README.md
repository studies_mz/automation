## FLASK
Projekt tworzony na zajęcia automatyzacji

### Dlaczego README.md jest ważne 
Zawiera instrukcję instalacji, uruchomienia środowiska, deployu aplikacji na produkcję. Wszyskie niezbędne komendy. Wymagania do uruchomienia projektu. 
## 🛠 Instalacja
Instalacja bibliotek
 ```
 make install
 ```
 Uruchomienie serwera Flask
  ```
 make run
 ```
 Sprawdzanie składni
  ```
 make pylint
 ```