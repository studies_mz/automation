.PHONY: install
install:
	@pip3 install -r requirements.txt

.PHONY: run
run:
	@python3 -m flask run

.PHONY: pylint
pylint: 
	@pylint app.py